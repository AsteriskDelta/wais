NAME = WAIS

#Directory that contains .cpp files with main()
EXEC_DIRS = ./specs/

#directories with classes/files to be compiled into objects
SRC_DIRS = 

#directories containing headers that need to be installed
HEADER_DIRS = app e6b gis include web well
HEADER_CP_DIRS = 
HEADER_FILES +=
#Header install subdirectory (ie, in /usr/include: defaults to $(SYS_NAME))
HEADERS_OUT_DIR = WAIS

#Choose ONE header, if any, to precompile and cache (not for developement!!!)
#PCH = iWAIS.h

#Default platform
TARGET_PLATFORM ?= Desktop
#Local build output directory
BUILD_DIR = build

#Compiler
CXX ?= g++
#CFLAGS (appended to required ones)
CXXPLUS += `sdl2-config --cflags` `freetype-config --cflags` -fopenmp -fno-stack-protector
#SYS_FLAGS (prefix and possible override system CFLAGS, may break things)
CC_SYS_FLAGS ?= -Wmaybe-uninitialized -Wuninitialized
#Optimization flags, supporting PGO if needed
OPTI ?= -march=native -Ofast -ffast-math -fno-strict-aliasing
#Include paths, ie -I/path/to/headers/
INCPATH += -I/usr/include/TMSC/ -I./
#Libraries, ie -lopenmp
#LIB_PATHS += ~/Projects/Dimensionals/build/Desktop/ ~/Projects/GNFCtrl/build/Desktop/
LIBS += -lGL -lGLEW -lGLU `sdl2-config --libs` -L./dyn/ -lassimp -lfreetype -lnoise

include /usr/include/LPBT.mk

install::

uninstall::

autorun::

disable::


