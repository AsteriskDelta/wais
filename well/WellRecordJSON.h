#ifndef WAIS_WELL_RECORD_JSON_H
#define WAIS_WELL_RECORD_JSON_H
#include "iWAIS.h"
#include "WellRecord"

namespace WAIS {
    class WellRecordJSON : public WellRecord {
    public:
        WellRecordJSON();
        virtual ~WellRecordJSON();

        virtual bool parse(const std::string& rawJson);
    protected:

    };
};

#endif
