#ifndef WAIS_METAR_STATION_H
#define WAIS_METAR_STATION_H

namespace WAIS {
    class MetarStation : public WellSource {
    public:
        MetarStation();
        virtual ~MetarStation();
        
        std::string id, site;
    protected:
        
    };
};

#endif

