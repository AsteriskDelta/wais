#ifndef WAIS_METAR_RECORD_H
#define WAIS_METAR_RECORD_H
#include "iWAIS.h"
#include "WellRecordJSON.h"

namespace WAIS {
    class MetarRecord : public WellRecordJSON {
    public:
        MetarRecord();
        virtual ~MetarRecord();

        Station *station;
        Time time;
        Vec2 position;

        Temperature temperature, dewpoint;
        Vec2 wind;

        MetarCover cover;
        Distance visibility;

        FlightCatagory catagory;

        Pressure altimeter, seaLevelPressure;

        std::string raw, taf;
    protected:

    };
};

#endif
