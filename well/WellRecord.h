#ifndef WAIS_WELL_RECORD_H
#define WAIS_WELL_RECORD_H
#include "iWAIS.h"

namespace WAIS {
    class WellRecord; {
    public:
        WellRecord();
        virtual ~WellRecord();

        Well *owner;
    protected:

    };
};

#endif
