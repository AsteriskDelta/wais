#ifndef WAIS_WELL_SOURCE_H
#define WAIS_WELL_SOURCE_H
#include "iWAIS.h"

namespace WAIS {
    class WellSource {
    public:
        WellSource();
        virtual ~WellSource();
        
        std::string id, name;
        Well *owner;
    protected:
        
    };
};

#endif
