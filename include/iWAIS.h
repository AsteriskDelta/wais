#ifndef INC_WAIS_I_H
#define INC_WAIS_I_H
#include <NVX/NVX.h>
using namespace nvx;
#include <Spinster/Spinster.h>
#include <ARKE/ARKE.h>

namespace WAIS {
    using namespace arke;
    
    typedef NVX2<2, nvxi_t> Vec2;
    typedef NVX2<3, nvxi_t> Vec3;
    typedef nvxi_t nvxi;
    
    typedef nvxi Temperature;
    typedef nvxi Pressure;
};

#endif
